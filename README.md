## Clone the project

This project is hosted in bitbucket.org

Clone the project

Copy the option to clone the project

```
git clone https://<user>@bitbucket.org/ewemat/task.git
```

## Run the project

```
# npm install && node index.js
```

## Example use

The code can be tested with curl, postman and probably in many more ways depending on the operating system

On Linux RHEL7 with curl installed:

Open terminal, in first tab: 
```
# npm install && node index.js
```

in second tab:

Try to generate key pair:
```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGVAbWFpbC5jb20iLCJpYXQiOjE2MTQ2MjIxMTksImV4cCI6MTYxNDYyMjcxOX0.fZZKv6WCwvErlBsc2XcpMLiQuR65JulwIy156_7gmao" -X POST http://localhost:3200/api/generate-key-pair
```

Output:
```
"No users exist. Please sign in."
```

Try to encrypt the file:
```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGVAbWFpbC5jb20iLCJpYXQiOjE2MTQ2MjIxMTksImV4cCI6MTYxNDYyMjcxOX0.fZZKv6WCwvErlBsc2XcpMLiQuR65JulwIy156_7gmao" -X POST http://localhost:3200/api/encrypt
```

Result:
```
"No users exist. Please sign in."
```

Sign in
```
curl -d '{"email":"example@mail.com", "password":"1234"}' -H "Content-Type: application/json" -X POST http://localhost:3200/api/sign-in
```

Received result:
```
{"authToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGVAbWFpbC5jb20iLCJpYXQiOjE2MTQ2MjIxMTksImV4cCI6MTYxNDYyMjcxOX0.fZZKv6WCwvErlBsc2XcpMLiQuR65JulwIy156_7gmao"}
```

Next sign in with another user
```
curl -d '{"email":"example2@mail.com", "password":"1234"}' -H "Content-Type: application/json" -X POST http://localhost:3200/api/sign-in
```

The result:
```
{"authToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGUyQG1haWwuY29tIiwiaWF0IjoxNjE0NjIyMTg1LCJleHAiOjE2MTQ2MjI3ODV9.Grcy_10t70dbPAf6fdHeBlUiQpf37BvQ2aAVuDf0-RI"}
```

Generate key pair for first user:
```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGVAbWFpbC5jb20iLCJpYXQiOjE2MTQ2MjIxMTksImV4cCI6MTYxNDYyMjcxOX0.fZZKv6WCwvErlBsc2XcpMLiQuR65JulwIy156_7gmao" -X POST http://localhost:3200/api/generate-key-pair
```

Output:
```
{"privKey":"-----BEGIN PRIVATE KEY-----\nMIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAKwP6r2LjYOGPRbV\nNR0RQDI5cE/+PrHOiSSGr9BLCfRjYGoBoSkEk/CAFXUKWrLEFQYC5UMqF82RDATM\ndBtZ5n49ol13q5Bq4J5rJqy81JJ3NiMygyROOyfNqrsm7O3wqM5Vzt9sdaQdpN6R\nHCPHXlw0AwmOx2E0tUvzTuvKMoflAgMBAAECgYEAi4EcQjIzur/MKAcV2UtzUUVg\n2nq5OlDKwOKzCZ7No2gvc8N+d1NO31/6BbsvbmGs6gLO/3XRdspzhDCAsuxhBAT/\nJrKbwHTsgoqyQ7oZv4YIBojC6iOz3PeLNaXCZ5iOXGt4Po1VTfaHMyxyqf9UJu8X\nnBYwBx5nGw9X2JSKXMECQQDdnP57wXzOpkqtFlFNYKk0y0Xf5eY4wXKn5pQyzUMr\niInzb7272nhQfRN9/q8+KrdGfeukb2x9pHSZE5dSuidzAkEAxsKiz4BgxPGF0Cfg\nMZDFke7Z2Z87ldT5KKfz12jBsEjKO+I0jIL9L1ZOVU2aOJRbJEHr+YxPTDv2njT4\nhXxNRwJBAI8FPbsykNBww2rXJ1svkaPPt+WaYiyos80l4uiyTbIvqQ/hQmqUwOb5\nAXZjwkb1nFgy9bumaTbgxprv6M3r838CQGrr01easPPhFWH3wYQIZC/v9GM8ZHpi\nmjJDv0nWwbMKRThXpHseEANCA1uiJl9fd3QWvlF9KD9TVoSe7D4qswsCQQCXX2Rt\nUerEkJJOhOyCtXiN/E/FfJI2ugUkfSt4dBmkqW8bkXtqEdGpWZqmsXDaBUHpb5AH\nlaAs6TBk39IJMki1\n-----END PRIVATE KEY-----\n","pubKey":"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCsD+q9i42Dhj0W1TUdEUAyOXBP\n/j6xzokkhq/QSwn0Y2BqAaEpBJPwgBV1ClqyxBUGAuVDKhfNkQwEzHQbWeZ+PaJd\nd6uQauCeayasvNSSdzYjMoMkTjsnzaq7Juzt8KjOVc7fbHWkHaTekRwjx15cNAMJ\njsdhNLVL807ryjKH5QIDAQAB\n-----END PUBLIC KEY-----\n"}
```

Generate key pair without jwt:
```
curl -H "Content-Type: application/json" -X POST http://localhost:3200/api/generate-key-pair
```

Output:
```
500
```

Generate key pair for unknown jwt:
```
curl -H "Content-Type: application/jsonH "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGVAbWFpbC5jb20iLCJpYXQiOjE2MTQ2MjExNTgsImV4cCI6MTYxNDYyNDc1OH0.LBuhhSZYhsxTAnblyWAhjlR7mtljekh3Yz-8zTjR7EA" -X POST http://localhost:3200/api/generate-key-pair
```

Result:
```
401
```

Generate key pair for second user:
```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGUyQG1haWwuY29tIiwiaWF0IjoxNjE0NjIyMTg1LCJleHAiOjE2MTQ2MjI3ODV9.Grcy_10t70dbPAf6fdHeBlUiQpf37BvQ2aAVuDf0-RI" -X POST http://localhost:3200/api/generate-key-pair
```

Output:
```
{"privKey":"-----BEGIN PRIVATE KEY-----\nMIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAK/a6YPO0uXqyO3J\n5OR//zX1fqJmP096KN7oLvPB0MArtt1Krx3vcodPTEMkFtJaJJtZjAbaakZNXccC\nDCEQhs5EAVx6z7ev1lTNtJgj/OWu3nfFFtDxz8CZg7W1jvIJLJk4+5v6rBqZ6EPv\nXGDmg0ikfP1fkH6+VhF5EONxVRMXAgMBAAECgYAO11/b0umzo8P/ZYmeyzqjuCtO\n02hGcd3fKiNESTgv78CIHpR25014I/YguHwAlsD+J2mM7oQHmOHwOLkDVVy7CdOd\nh57tgUT+MCVbo9/RRRhF55fP6U+Rgmmg01S9hxdpbPkn2DADhk59I8hpCivMjRZu\nm44OHItwnBuoPpv/2QJBAOQw+K5MstEB8c/8oni3bG5SQra/T7r+1oJemlSehyp6\nTXsFWXsFnEfuKMmHDpKYMV9hTgyo/9meKOjh8lMT1bsCQQDFSS0UXt1tRUDrUX7Y\nhDqWPdj0tpig4MxgaDVkehMHkYx58WiHVxUZvZdoyBeoiLPm91G8rh5FDq9Ugpuq\nHZRVAkAj2lHoSOK7lhuhOydn4iHmP4R95Bcp4Upg125WJ0ZDdU3hK1kAzEPexVuh\nNlgizkjLnKU/tfnk/kWdOYryZKZrAkAIRnqCmTwWgkon1MJKWLmQZcW89g/O578K\n6DQdI9W6gotu7gZhbvCupLo+Je6AHkPVk1g1XuRcJzKJi/T1nMoFAkB+Ca4ioFdG\n4cgAk92GY92jPD1Oiqo/HuUp5Kra4UEvJsPrVYTzYnSle1e5zrsJyq/O/ChmfTRM\nE6Y82b2qm+UY\n-----END PRIVATE KEY-----\n","pubKey":"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCv2umDztLl6sjtyeTkf/819X6i\nZj9Peije6C7zwdDAK7bdSq8d73KHT0xDJBbSWiSbWYwG2mpGTV3HAgwhEIbORAFc\nes+3r9ZUzbSYI/zlrt53xRbQ8c/AmYO1tY7yCSyZOPub+qwamehD71xg5oNIpHz9\nX5B+vlYReRDjcVUTFwIDAQAB\n-----END PUBLIC KEY-----\n"}
```

Encrypt file content for first user:
```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGVAbWFpbC5jb20iLCJpYXQiOjE2MTQ2MjIxMTksImV4cCI6MTYxNDYyMjcxOX0.fZZKv6WCwvErlBsc2XcpMLiQuR65JulwIy156_7gmao" -X POST http://localhost:3200/api/encrypt
```

Result:
```
qdGIkV4Tdcj5e+tM4oirYnENETTL03MEJZBI4qdzxOdYgI3tk0aTysQEJIrDS711lXLA0ycwqU97f1Sxjvp07nsNaTxspqiiUanv9lBgjRZLFGTZu5eXO+BJj/O0fv075QrynhblNZn+IiyWA7/MglSs9yp+DEZYaEqGtbFiSgg=g5tlyKZsni4hCbsmuLRPX88VOW6UljjSN7wOHCgETH3X7by+AfF1u+Ze2xIf1yskVJ/O4eckNYN8iFmcLn9YiWcwwjxZ0Qfw3Rw9O3AKT9DjiIPsk0/2tIYnubb+2rPiyFNlM1mjnqnQRZxijWIAjBMwsuAI04Ip2PGGnNno9hY=hrMwQpuooa0YMr+bKBZuYIPfG8BpAoKs0DPHW314rw8KF03bINN51BODV3xNjwVGrO/b/ARqFQUtC3ArsFCgCbk6dQc32dsv54VIcDu++8cXlYObiLQSCi+TeoRtjK33WynQGe/M6gLbO4/7egW0s/fHamQ+zrz6i+TvABJ8wVA=Rci9B5fbTVVfoFjIz4sdqkp8pZBRPbbVTMM7PqLLrKKibgnr3aYQ8h+Yc3xPymXHH8fNAwT2+tOcQw5BfW8nqO0EzGqFrynP8kC8V3cdETOGDpAKWvgzaZxXUNOcK7FgEwBqaHhRJKONk2gcU508VfJf0GzJOzuAC52SvLegW5g=bJWB9Uskps7nZ3ZL0uWLm4uZegaepmS/Bkt9bCBRTtfqcV9cGqoRZc+nzNMgzVRNUMlc1bwQV8WFaVT4UJXXcnwQPrevL2NeODKGMYguQNQviL8z3b2tWL3KmBtF//TbnSVfz06pBZ7BCCfUEf6lwdrnj71kPBtpL3EU+AFIlIc=bUzXcQjfjyeIr6+NtEoNaCa4+D0Rtir+4Td/7eid4AVXMSBYlAqPHBRiSEArZgrxlLkrbHPm7WAxRolAoDXD620P5CIKJa+CFO5o1trUfyJDfsENjdbkwfC9NA6lFaM07NzsP+0Y24Qa8Xu1vkExSt2bFJjcWm1fR1EQtCXMFu0=p8c5xiur0y12voyCAvNlxZLaST8DMlm3az+cA1OumiD7hwAhhhwkEf914DIPVURqQNp/tkjNVDvPmKYH9pFv/FvjDLwsvxlh6i0ZMZXwOw/VfhyHXVXvTcEqezlRZ/ZCcVNU8PK6VPZI8wiOVmlsuQL+Btj5XZk3e3BDC/jRLzM=J0QUCv2gGr7y1WqscJZB3T5IlGUSGVcXBCuf8/l7c2mnJh3TKScnbbsiNw5952aKTX9sMYdEL0S02+rUtIVmRTKFQHEf/+AGayHzw9dVDrhAf3ginTl02GSdIe2gRkAUyiON6m8TYcJLyf7XuXRhGUNjRmWjP+nCHVR0X4TrHjw=CG1n+OpXqCBFGLTLL/A9Ty3A3YkXcVq5FnKdukLnIEDgx0Maf87/9yau4YCVi8PdloTpikj/Yhm9+3Pd6rMSJhMIoGmTHXkpYuCLI06SPnRc8TwfyxVKNKNalYNFzIxyhJixR16zMCpXY5fmDPpo08XesqetiGtEeWZKY9Qvf4o=A/G/cPABlBXisT9YYAHSjB22XRc7mSvqQahqN3N3jex0azrwxOv7f5NAfl7WT7dGlFaRuMp0ceWhkZv5/FPABfZPPtXu+zTBRslmYU+106PuN01zFrEQkqB3yEYD5XwSn22B105UIS/D1/Oo/GeNGJkcGV4MB2IStLbzv7R2u7E=ns0aacivLWNOhhjx2pEeSlOo8+w+kpxKNiFKkxmKxzaTb7pIn9Wik/GjLprH7U6nGGHy+oN5hLcAuWrDCk7VbADic8azyWZWLvajM/igC6T5DXTEnc212CihW80SVVEgz3WeASVe/Q/6YAPzgstdUY1y8xGemOt3tv6r5f71gew=cORK6XYUdP222+Rt7v05McUqbuQ4fASJ54taxvE30v1wP/dy+uLmbUP9xm7QPq3leYMP6h7Zvp2oju1VmYK2thUKLiLyYu87GdGVKIRudlgg7C6FEDon8xyiidMZ/n6bt0am4cJPgEWpc1UFskCbxNRsFMHKiTT+OR3UleR4qzA=bFx482vgmcUz5rLM71RWeMizL6gRHKwADRlWfSdDOwafZV+fpdzaCV6owZYMH+NA72phJf/v19KwPqtUoJzV2C1szjn4CQZIJfvyg+K5T8N2FAjquS1SfLp6UI6m4O98ZZ1fG2GZOJmwNGkylCR6PKO8Dcj070IqRhtBNG3OK9Q=dFUPWNtNYUDG0rH2hU2V7l2kvQDJtlKErrS28ZiHR/o2SeLh7LN+6LuIb9MB2PZjuoxM9gdfKGmBNckvL45fAA4dCq+bO+qZyERO4Wv5JTgVhHRVme6y3AHGisrwjZs5Oi3xHQv5yiz1TQwm3F9gOEELD5TdAeETfCsRIblXiTM=f0D03YNHXx2ok9+LLzBY3zC0CRk2HJJccEX8nsPlYHv8mtkHWHUHbzw6F67RrYa6mQQUn5UGGrW5vMTgBG6Px+jx531RXKHPn8pqXr1C7Gk1EX3xVUFBQYw7fSztDk8p0xFtZ8Gi8HZEioA0puWzjUIhhKBqWxafqCWKBmUMGFI=Eod0cfAbqjJ833P8a6oSSY7zHYmUWBujDl6tC2XelTA1TCE17zLMYXnlHUB8qB85VayxqHDmModPJXsAejZQWlDfzfDb86QwFIWz5MAtVwxdfq+St9yEaiR0eDbabl/BVCEQ2yTTAu0miXGekaUnxHDdI43S/Z1SNOyDjvl3SQE=
```

Encrypt file content for second user:
```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGUyQG1haWwuY29tIiwiaWF0IjoxNjE0NjIyMTg1LCJleHAiOjE2MTQ2MjI3ODV9.Grcy_10t70dbPAf6fdHeBlUiQpf37BvQ2aAVuDf0-RI" -X POST http://localhost:3200/api/encrypt
```

Result:
```
V7hffdIuSZCP3abslaPgduxkvp9VsBbijSxfxLDrCjp/f+JTBYDs4pQ3cs8VaEu4OwCKMVICJtN94cZDCT7QZ7wUxi2Nc9D+1ZXLEJMArUUib4bIrLp9AMAss9Acc3qcM7giJwbbwMaQx9PPJqmZhIjuPkf/udI9qD9WwozKnJQ=SD09/113EpJgl2o08NAuwFB3gw0jPruZQxEWjcGTeOECuk3EfInUcYfcLWp3mJRcd2iG31TBD5xNdStgqUqQCFxkFruP0DatG+df+tGdM1mrE7/xbzd02hQ7qbsOmjdCBOjFB4qnQOWyoFkRWPdHKVPWIfPUwHuETmEYZI8ABQw=KokuFmcVVRjZaM9xcnInDbJV/EJojr7JJy3Sak2y2EmjKI5k0GIn/ECuh10bvRb0gwMTLtPo+9AybIksyuVMfgb0fHFg7QuqWTfMYNEutisYIwGyin+UnjVEF0z06jSfM6gGRd/ZQAB4G4k7RtziXTCwb+8hMFTLn5kbYG88PoI=YW4d3wgfJr+bbSfx+HimtUvuTvJCDGAiDam00qx5ZDjFz14v6xu5AVVioXAwebGiialti2LvrHsMwmYLFfvLHvozRDM7iKZkKE8o3CXez53cD3A4rhemgsl/Z+qB13x3jZ0E78UN8wgLtBuKOvTXnYTdcJJ2JMHF9emBtOdyMeo=V+i2fDtMi3caJl+8r/AhBOo5Xyt1G4a9lB5mlZYoiUBMBG6w4Nu86gEiNkD6kkgHK1x+ItaWarwPoYF35SJOJVDj64uzsU73ws639UciuA3PCLxX1p8vpyvGFNlYUJLvyi6lS5l6xYEzsvOSxfdAEsKw0RC3ilWxTSmLM75BX24=dQnLvJzICq79BE/hyR+tHLW9jfeB0FlFtBFHjazMMsX+zcytlfi9FBHH2JCV1phH/vJWcmXjZgQT5bY7Ld839yO1Ykz75VmWLDrY1qHeF2fCaYZlW0lBWQiC3Vyv2YFUG6fVA8bUu6ZZJLGnNWc5kJwd6uwAg88u/iEcNl+Ha8E=Q5qF2JbFYs4zvJrlX/YfEpinZXX8u1QHvkau2Rpu0fj1f/YF5uoqQo8j5wBAxBXVq1KHpeErdAEE+xit3pqK7rqsJRdm79q76FuTrN66UXbtxf4rsL2ZG0xLOY/7hRYfayG0yXXV6ut+qDn4s/48XnLKAF7xk2y4AWWxHdK2OfA=afYqK4nKpIY7X4m1mwuPcdCGC7O3X6BcGyKInpyJRuPDZFXmE5ArH3LRa/F7RCshAir162yPsMIE+8oqdFdFb49b2KXznOjrPyJMJsx+ELOF3ONCuW06fpQYmDE0L7BdohVnRQK7xz09XvztJ6+n+/YuDBCMt8xd4qXUsLhxyr8=fdieY0+a7D4KhT3BLYLhbPasoCiY8JjMR8y25kak5hMAfjoWPoEqFPhl2qdPM+zJ86tSlyVpi3BCmU676qwE+nx7S/JYbYJUdkM+iyerYYFsRBUrinbhIlFWxTi03S1dLLcGR/vlMucxm0CrSFD6TfnP4B1Cn6dQiw3TmPI06LU=myL1fNkHXARGxhQEF03mztkvA013llMIMFcpxeSU7Yghwg4AqqUTuUUa3Y4C98Z++eoTKo+K2Nd771x4NTmb9yIryE/w/W0j92PkDRuc/niOi55ac2EAIdjQ3f6kVKubdh+ObYBdMgEQIP8NfA0LHAgfk1GydjSq3Sv0sG8Dmww=SGeo0q+vy/JFdGOeCVpl5gdrXW1N+G/4BiAkk4s7AEG+F4Le6zm0RVZApVKmFRlU9mB0jIrwAO9z163aXf30utw+WYzMo2NnijjeEUh2a8et976qFsqxD/gwrUwfT5vtUdvs7wx7TuTIB7pb5yuv4BoEOCE/HOKwcUIQbhVk0/o=XbHsVFeGkrRrcNB5yZSjDenq2WV5OxIVKzV0T+F8mrm0+YkPeF8DyycwEsFrFGXpMkvY18AWlFOBRVpWelE/ZKSHFJMYp151U8qA3op+nE6tcSuWu3h3l1eAMXkGlosXAIMCsIzrBxnbbUIm65S94YoHDZGUv202WOGkzAzZVFE=Q6y/SYZTQXj0LOn860l+kfgmD1yCTEse+QP/2cQzDuruqf9wv7s6drv1+DThfGwiRfsk104EEj5NPfEZCoBlHTGEzIGDdNzUu4Fo1YUK1RHTD1/06twvBCqvDMbL7GSkdrbVgR5EW4xeBuMR5kcBirDvYTkQOV0a51zHY7ix44I=LgO5n4czSTI/PGDJ9dAXZO7gYek7gOvT5kK95ONzOXZCX/6UZwZQIiKJwzu7bjk/+kcS7S/7kOYTbmPA4WWwB7m4INmOYZUJJZB/vslZX/rIUvCDJfaioy91HPUt23tcMj4bXtOcNfjDDHD1tO1uzbmIdZVJaEbPKiBcr0K+HC0=XVwtSeoNuheeJYNGiFVdnZ3tOL4NyJtXXMAF4ZeRo+n0uCAk2E2sAcgo9dG3EUEb9RVfnC6j8eYpq3woYURBcCfNaoygD9ZA7r9+ldNKq/FYow4yUvxJDXS6QGkB5q2E0fGJdCbvWWSK7GIxNHQ/ke8BayoMuwhawV+Vjk71OKA=Zfd7RGbVXMK3eqADPs6sXCkEErWzqCHBacn1xANiyBwS2Z9JxfvF547XQ5w3rAuoI/dHBtxRoW31S871pjnEuDoizZsals+j06Bm0l1geatHlb9YuOUOWbBx5Cr07RgXXx1cxufFcenuu18RHWXnN20IEEbQUDQ09e3qbRUuVrg=
```

Wait 5 minutes...
Encrypt file content for first user:
```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGVAbWFpbC5jb20iLCJpYXQiOjE2MTQ2MjIxMTksImV4cCI6MTYxNDYyMjcxOX0.fZZKv6WCwvErlBsc2XcpMLiQuR65JulwIy156_7gmao" -X POST http://localhost:3200/api/encrypt
```

Result:
```
401
```
