const express = require("express");
const bodyparser = require("body-parser");
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const pdfreader = require("pdfreader");

const app = express();
const port = process.env.PORT || 3200;
let users = {};

function returnStatus(err, res) {
  if (err.message === "jwt must be provided") {
    err.status = 500;
    res.status(500).json(err.status);
  }
  else {
    console.log(err);
    err.status = 401;
    res.status(401).json(err.status);
  }
}

function getTokenFromHeader(header) {
  return header ?
    header.split(" ")[1] :
    undefined;
}

function usersExist() {
  return Object.keys(users).length;
}

app.use(bodyparser.json());

app.post("/api/sign-in", (req, res) => {
  const email = req.body.email;
  const payload = { email };
  const token = jwt.sign(payload, 'secret', { expiresIn: '10m' });
  users[token] = {
    email,
    password: req.body.password
  }
  res.status(200).json({
    authToken: token
  });
});

app.post('/api/generate-key-pair', (req, res) => {
  if (!usersExist()) {
    res.status(404).json("No users exist. Please sign in.");
  }
  const token = getTokenFromHeader(req.headers.authorization);
  jwt.verify(token, 'secret', function (err) {
    if (!err) {
      const { privateKey, publicKey } = crypto.generateKeyPairSync(
        "rsa",
        {
          modulusLength: 1024,
          publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
          },
          privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem'
          }
        }
      );
      users[token].pubKey = publicKey;
      // users[token].privKey = privateKey;
      res.setHeader('Content-Type', 'application/json');
      res.setHeader('Authorization', token);
      res.send(JSON.stringify({
        "privKey": privateKey,
        "pubKey": publicKey
      }));
    } else {
      returnStatus(err, res);
    }
  })
});

app.post('/api/encrypt', function (req, res) {
  if (!usersExist()) {
    res.status(404).json("No users exist. Please sign in.");
  }
  const token = getTokenFromHeader(req.headers.authorization);
  jwt.verify(token, 'secret', function (err) {
    if (!err) {
      let content = "";

      new pdfreader.PdfReader().parseFileItems("sample.pdf", function (err, item) {
        if (err) {
          res.json(err);
        } else if (!item) {
          res.send(content);
        } else if (item.text) {
          const encrypted = crypto
            .publicEncrypt(users[token].pubKey, Buffer.from(item.text, "utf-8"));
          content += encrypted.toString("base64");
          // console.log(crypto.privateDecrypt(users[token].privKey, encrypted).toString());
        };
      });
    } else {
      returnStatus(err, res);
    }
  })
});

app.listen(port, () => {
  console.log(`running at port ${port}`);
});
